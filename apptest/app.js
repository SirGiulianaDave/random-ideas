//defining global variables
let createProfile = document.getElementById("ProfileButton")
let logo = document.getElementById("logo")


//Event listeners
createProfile.addEventListener("click", ProfileClick)

function ProfileClick() {
    console.log("profile clicked" + innerHeight)
    document.body.innerHTML = ''
    loadProfilePage()
    buttonCreator()
}

let profilePage = profilePageElements(` 
<div id="topBar">GetBetter</div>
    <br>
    <div id="phoneScreen"></div>
    <div id="boxParent">
        <label for="name">Your name: </label>
        <input type="text" id="nameId" class="textBoxes">
        <br>
        <label for="age">Age:</label>
        <input type="number" id="numberId" class="textBoxes">
        <br>
        <label for="location"> Location: </label>
        <input type="text" id="locationId" class="textBoxes">
    </div>


    <div id="buttonParent">
        <p>I am feeling:</p>
        <button class="feelingButtons" id="button1">Depressed</button>
        <button class="feelingButtons" id="button2">Anxious</button>
        <button class="feelingButtons" id="button3">Stressed</button>
        <br>
        <p>I have problems regarding: </p>
        <button class="feelingButtons" id="button4">Relationship</button>
        <button class="feelingButtons" id="button5">Family</button>
        <button class="feelingButtons" id="button6">Work</button>
        <button class="feelingButtons" id="button7">Study</button>
        <button class="feelingButtons" id="button8">Other</button>
    </div>

    <textarea id="bioInput" maxlength="256" rows="12" cols="50"></textarea>
    <button id="finishProfile">Create Profile!</button>

`);

function profilePageElements(html) {
    let profileTemplate = document.createElement("template");

    profileTemplate.innerHTML = html.trim();

    return profileTemplate.content.cloneNode(true)

}



function loadProfilePage() {
    document.body.appendChild(profilePage)
}

let mainPage = mainPageElements(`
<div id="logo">logo</div>

`);

function mainPageElements(html) {
    let mainTemplate = document.createElement("template");

    mainTemplate.innerHTML = html.trim();

    return mainTemplate.content.cloneNode(true)
}


let user = {
    "Name": "",
    "Age": "",
    "Location": "",
    "Depression": false,
    "Anxiety": false,
    "Stress": false,
    "Relationship": false,
    "Family": false,
    "Work": false,
    "Study": false,
    "Other": false,
    "Bio":""
};

function buttonCreator() {
    let nameInput = document.getElementById("nameId")
    let numberInput = document.getElementById("numberId")
    let locationInput = document.getElementById("locationId")
    let bioInput = document.getElementById("bioInput")

    let button1 = document.getElementById("button1")
    button1.addEventListener("click", button1Click)
    let x1 = 0
    function button1Click() {

        if (x1 == 0) {
            button1.style.backgroundColor = "rgb(79, 185, 127)"
            x1 = 1
            user.Depression = true;
        } else if (x1 == 1) {
            button1.style.backgroundColor = "white"
            x1 = 0
            user.Depression = false;
        }
    }

    let button2 = document.getElementById("button2")
    button2.addEventListener("click", button2Click)
    let x2 = 0
    function button2Click() {
        if (x2 == 0) {
            button2.style.backgroundColor = "rgb(79, 185, 127)"
            x2 = 1
            user.Anxiety = true;
        } else if (x2 == 1) {
            button2.style.backgroundColor = "white"
            x2 = 0
            user.Anxiety = false;
        }

    }

    let button3 = document.getElementById("button3")
    button3.addEventListener("click", button3Click)
    let x3 = 0
    function button3Click() {
        if (x3 == 0) {
            button3.style.backgroundColor = "rgb(79, 185, 127)"
            x3 = 1
            user.Stress = true;
        } else if (x3 == 1) {
            button3.style.backgroundColor = "white"
            x3 = 0
            user.Stress = false;
        }
    }

    let button4 = document.getElementById("button4")
    button4.addEventListener("click", button4Click)
    let x4 = 0
    function button4Click() {
        if (x4 == 0) {
            button4.style.backgroundColor = "rgb(79, 185, 127)"
            x4 = 1
            user.Relationship = true;
        } else if (x4 == 1) {
            button4.style.backgroundColor = "white"
            x4 = 0
            user.Relationship = false;
        }
    }

    let button5 = document.getElementById("button5")
    button5.addEventListener("click", button5Click)
    let x5 = 0
    function button5Click() {
        if (x5 == 0) {
            button5.style.backgroundColor = "rgb(79, 185, 127)"
            x5 = 1
            user.Family = true;
        } else if (x5 == 1) {
            button5.style.backgroundColor = "white"
            x5 = 0
            user.Family = false;

        }
    }

    let button6 = document.getElementById("button6")
    button6.addEventListener("click", button6Click)
    let x6 = 0
    function button6Click() {
        if (x6 == 0) {
            button6.style.backgroundColor = "rgb(79, 185, 127)"
            x6 = 1
            user.Work = true;
        } else if (x6 == 1) {
            button6.style.backgroundColor = "white"
            x6 = 0
            user.Work = false;
        }
    }

    let button7 = document.getElementById("button7")
    button7.addEventListener("click", button7Click)
    let x7 = 0
    function button7Click() {
        if (x7 == 0) {
            button7.style.backgroundColor = "rgb(79, 185, 127)"
            x7 = 1
            user.Study = true;
        } else if (x7 == 1) {
            button7.style.backgroundColor = "white"
            x7 = 0
            user.Study = false;
        }
    }

    let button8 = document.getElementById("button8")
    button8.addEventListener("click", button8Click)
    let x8 = 0
    function button8Click() {
        if (x8 == 0) {
            button8.style.backgroundColor = "rgb(79, 185, 127)"
            x8 = 1
            user.Other = true;
            
        } else if (x8 == 1) {
            button8.style.backgroundColor = "white"
            x8 = 0
            user.Other = false;
            
        }
    }

    let createProfileButton = document.getElementById("finishProfile")
    createProfileButton.addEventListener("click", saveProfile)
    function saveProfile() {
        console.log(user)
        user.Name = nameInput.value
        user.Age = numberInput.value
        user.Location = locationInput.value
        user.Bio = bioInput.value

        document.body.innerHTML = ''
    }
}

